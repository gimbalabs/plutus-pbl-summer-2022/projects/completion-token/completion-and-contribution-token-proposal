# PPBL Course Completion Token and GBTE Contribution Token

The purpose of Plutus PBL is to prepare people to contribute to projects building on Cardano.

## Outcomes:
- In order to create the first instance of [Gimbal Bounty Treasury Escrow](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus) (GBTE) on Mainnet, we need to define a Contribution Token. One goal of this proposal is to define the relationship between Completion of the Plutus PBL Course and Contributor access to the first GBTE.
- The PPBL Course Completion Token is also the first instance of a tool that will be build in different ways in the future. Another goal of this project is to create a [generalist template-able outcome](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/completion-token/completion-and-contribution-token-proposal/-/issues/1#note_1126679464) that can be re-implemented with different rules in the future.
- This token should attest to the completion of Plutus PBL Course V2 (Summer2022).
- This token will be used to grant "Contribution Access" to the [first instance of GBTE](https://plutus-pbl-summer-2022-projects-gbte-gbte-front-end-x5ja.vercel.app/), which like the Completion Token, is designed to be re-implemented by different stakeholders with different goals.

## Requesting Your Feedback:
- Please add your comments and questions for this proposal in this [Issue](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/completion-token/completion-and-contribution-token-proposal/-/issues/1).


## Proposal Contents
1. How to earn a token
2. Token Metadata
3. Token Unlocks Contribution at GBTE
4. Additional Access will Follow
5. Call to Action: Image for Completion Token
6. Process for Requesting a Completion Token
7. Token Name
8. Additional Questions

---

## 1. How to earn a token
Plutus PBL students can earn a PPBL Course Completion token by completing all three Onboarding & BBK Assignments, and then by meeting of the two options for demonstrating mastery on 300-level Modules.

### 1a. Onboarding & BBK
Students must complete these Assignments to earn a Completion Token:
- [102.4 Assignment: Build Your First Transaction](https://gimbalabs.instructure.com/courses/26/assignments/442)
- [201 Completion Assignment: Mint a Token](https://gimbalabs.instructure.com/courses/26/assignments/451)
- [203 Completion Assignment: Hello Testnet!](https://gimbalabs.instructure.com/courses/26/assignments/448)

### 1b. Mastery on 300-Level Modules: Acheieve at least Option 1 or Option 2

#### 1b - Option 1:
At minimum, students must complete the first two Leveled Mastery Assignments on Modules 301, 302, and 303. This means:
- On Module 301: [Unlock tgimbals from Faucet on Pre-Production](https://gimbalabs.instructure.com/courses/26/assignments/459) and [Create and Test Your Own Faucet Instance](https://gimbalabs.instructure.com/courses/26/assignments/458)
- On Module 302: [Customize and Style a Query](https://gimbalabs.instructure.com/courses/26/assignments/465) and [Rewrite Koios Query with GraphQL](https://gimbalabs.instructure.com/courses/26/assignments/466)
- On Module 303: [Create a Bounty Commitment with cardano-cli](https://gimbalabs.instructure.com/courses/26/assignments/461) and [Create and test your own instance of GBTE](https://gimbalabs.instructure.com/courses/26/assignments/462)


#### 1b - Option 2:
At minimum, students must complete all four Mastery Assignments on Module 301, 302, or 303.

#### For Both Options:
Students can also choose to exceed the requirements. Progress will be recorded in the minting metadata of each Course Completion token.

---

## 2. Token Metadata
Here is the general structure of the Completion Token metadata:

```
{
    "721": {
        <NFT metadata>
    },
    "<another metadata key>": {
        "courseName": <string>,
        "courseVersion": <string>,
        "courseProvider": [<string>],
        "masteryLevels": [ // consider using ModuleMastery
            {"<module_number>": <string>},
            {"<module_number>": <string>},
            {"<module_number>": <string>},
            <additional_modules>
        ],
        "url": <url>
    }
}
```

For example, the metadata could look like this:

```
{
    "721": {
        <NFT metadata>
    },
    "<another metadata key>": {
        "courseName": "Plutus PBL",
        "courseVersion": "ppbl-course-02, Summer2022",
        "courseProvider": ["Gimbalabs", "Bob"],
        "masteryLevels": [
            {"301": "4"},
            {"302": "2"},
            {"303": "3"}
        ],
        "url": "https://gimbalabs.com/mastery-policy" // must include course Outcomes, Objectives/LTs, Modules, Team
    }
}
```
- [ ] When Approved: create a page at https://gimbalabs.com/mastery-policy. This page must show course objectives and learning targets, including topics covered in each module.

## Metadata can be updated:
When students reach a higher level. Future: build contract for automatically updating a Completion Token.

## 3. Token Unlocks Contribution at GBTE
Anyone who holds a PPBL Course Completion token will be able to complete Bounties at on the intial Mainnet release of Gimbal Bounty Treasury Escrow (Mainnet URL to follow). A Pre-Production example can be [viewed here](https://plutus-pbl-summer-2022-projects-gbte-gbte-front-end-x5ja.vercel.app/), and Test Contribution Tokens are [available here](https://docs.google.com/forms/d/e/1FAIpQLSffNGQdMqqr3mp1WfxyfexR7BxHXbsbCHIpCnYt2XAM3aYgSQ/viewform).

## 4. Additional Access will Follow
Future instances of Gimbal Bounty Treasury Escrow will be funded and run by different "Treasury Issuers". Every Issuer will be able to create their own rules for how someone earns a Contributor Token. Issuers might choose to:
- Use the original PPBL Completion token as a Contributor Token, OR
- Allow holders of a Completion token with certain Mastery Levels to mint a new Contributor Token. (For example, only for students who reached Mastery Level 4 on all Modules.), OR
- Create their own rules for minting Contributor Tokens

Gimbalabs will apply different scenarios to different projects. The initial Beta release of GBTE is built emphasize education and repuation-building, so the requirements are kept minimal.

## 5. Call to Action: Image for Completion Token
- We can choose to add an image to each Completion Token by applying the CIP-25 standard. If anyone is interested in contributing artwork, please [join the conversation on Gimbalabs Discord (links to forum discussion)](https://discord.com/channels/767416282198835220/1025440228627980330).
- The color of the token can change based on Mastery Levels: should we plan on this?
    1. How many different image / colors do we need?
    2. What are the minting rules for each?

## 6. Process for Requesting a Completion Token
- A PPBL student can request a Completion Token any time after they complete the minimum requirements described in Section 1 above.
- The process for requesting a Completion Token will be posted on Canvas.
- Students can wait until they have their desired Mastery Levels before requesting a token, or they can mint one and "upgrade" it later, as Mastery levels are added.
- [Placholder link on Canvas](https://gimbalabs.instructure.com/courses/26/pages/get-your-ppbl-course-completion-token)

## 7. Token Name:
Key question: Is the Token Name a sufficient identifier to distinguish one Contributor from another?
- For example, suppose the token name is: `<contributor name/alias>PPBLCompletion`
- Should the metadata [include information about the participant](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/completion-token/completion-and-contribution-token-proposal/-/issues/1#note_1121318865)?

## 8. Additional Questions:
- How can we make it possible to query all certificate tokens from Gimbalabs?